<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    public function index()
    {
        $employees = Employee::getAllEmplyees();
        return view('employees.index', compact('employees'));
    }

    public function edit($id)
    {
        $employee = Employee::getEmployeeById($id);
        return view('employees.edit', compact('employee'));
    }

    public function update(Request $request)
    {
        try
        {
            DB::beginTransaction();

            $request->validate([
                'name' => 'string|required',
                'age' => 'integer|required',
                'join_date' => 'required'
            ]);

            Employee::updateRecord($request);

            return redirect('/')->with('message', 'Employee record updated successfully!');
        }
        catch (\Exception $exception)
        {
            DB::rollBack();
        }
        finally
        {
            DB::commit();
        }
    }
}
