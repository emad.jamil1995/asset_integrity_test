<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $guarded = [''];

    public static function getAllEmplyees()
    {
        return self::all();
    }

    public static function getEmployeeById($id)
    {
        return self::where('id', $id)->first();
    }

    public static function updateRecord($data)
    {
        self::where('id', $data['id'])->update([
           'name' => $data['name'],
           'age' => $data['age'],
           'join_date' => $data['join_date']
        ]);
    }
}
